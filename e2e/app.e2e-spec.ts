import { Q2ClientPage } from './app.po';

describe('q2-client App', function() {
  let page: Q2ClientPage;

  beforeEach(() => {
    page = new Q2ClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
