
import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';

import { IUser } from '../user/user.interface';
import { _buildHeaders, _buildSearchParams } from '../../utils/remoteUtil';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Form header
const formHeader = {
  'Content-Type': 'application/json; charset=UTF-8'
}

@Injectable()
export class SessionService {

  //Moverlo a env
  ENDPOINT = 'http://www.giwiro.com:8080';

  constructor(private http:Http) { }

  private _createPostObservable<T>(url:String, data?:Object, headers?:Object): Observable<any> {

    const requestOptions = new RequestOptions({
      headers: _buildHeaders(headers ? headers : {}),
      withCredentials: true
    })

    return this.http
      .post(this.ENDPOINT + url, data ? data : {}, requestOptions)
      .map( (res:Response) => <T>res.json() )
      //.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  private _createGetObservable<T>(url:String, params?:Object, headers?:Object): Observable<any> {

    const requestOptions = new RequestOptions({
      headers: _buildHeaders(headers ? headers : {}),
      search: _buildSearchParams(params ? params : {})
    })

    return this.http
      .get(this.ENDPOINT + url, requestOptions)
      .map( (res:Response) => <T>res.json() )
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  serviceLogin(params: Object) {
    return this._createPostObservable<IReplyLogin>('/api/authenticate', params);
  }

  serviceLoginFB(token: string) {
    return this._createPostObservable<IReplyLogin>('/api/fblogin', {token})
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));;
  }

  serviceLogout() {
    return this._createGetObservable<IReplyLogout>('/api/logout');
  }
  
}

export interface IReplyLogin {
  success: boolean,
  user: IUser
}

export interface IReplyLogout {
  success: boolean,
  message: string
}