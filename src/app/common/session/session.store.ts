import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs/Rx';
import { IUser } from '../user/user.interface';
import { List } from 'immutable';
import { CookieService } from 'angular2-cookie/core';
const Promise = require("bluebird");
//import { Modal } from 'angular2-modal/plugins/vex';

import { SessionService, IReplyLogin, IReplyLogout } from './session.service';
import { UserService, IReplyRegistro } from '../user/user.service';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

const STORAGE_USER_KEY = 'user';

@Injectable()
export class SessionStore{

  private _loggedUser: BehaviorSubject<IUser>;

  constructor(private sessionService: SessionService, private userService: UserService, private cookieService: CookieService) {

    // TODO: Change localStorage engine for server side render user obj (more secure, faster)
    let user;
    try {
      user = JSON.parse(localStorage.getItem(STORAGE_USER_KEY));
    }catch(e) {
      user = undefined;
    }
    this._loggedUser = new BehaviorSubject<IUser>(user);
  }

  performLogin(data: Object) {
    return new Promise( (resolve, reject) => {
      this.sessionService.serviceLogin(data)
        .catch((error: any) => {
          reject(new Error(error.json().message));
          return Observable.throw(error.json().message || 'Server error')
        })
        .subscribe( (reply:IReplyLogin) => {
          if (reply.success) {
            resolve(reply);
            localStorage.setItem(STORAGE_USER_KEY, JSON.stringify(reply.user));
            return this._loggedUser.next(reply.user);
          }
        })
    })
  }

  performLoginFB(token: string) {
    return new Promise( (resolve, reject) => {
      this.sessionService.serviceLoginFB(token)
        .catch( (error: any) => {
          reject(new Error('Error al hacer loggin por Facebook'));
          //console.log('error performeando loginFB', error);
          return Observable.throw(error.json().message || 'Server error')
        })
        .subscribe( (reply:IReplyLogin) => {
          if (reply.success) {
            resolve(reply);
            localStorage.setItem(STORAGE_USER_KEY, JSON.stringify(reply.user));
            return this._loggedUser.next(reply.user);
          }
        } );

    })
  }

  performLogout() {
    return this.sessionService.serviceLogout()
      .subscribe( (reply:IReplyLogout) => {
        localStorage.removeItem(STORAGE_USER_KEY);
        this._loggedUser.next(undefined)
      } );
  }

  performRegistro(data: Object) {
    return new Promise( (resolve, reject) => {
      this.userService.serviceRegistro(data)
        .catch( (error: any) => {
          reject(new Error('Error al Registrarse'));
          //console.log('error performeando loginFB', error);
          return Observable.throw(error.json().message || 'Server error')
        })
        .subscribe( (reply: IReplyRegistro) => {
          if (reply.success) {
            resolve(reply);
            localStorage.setItem(STORAGE_USER_KEY, JSON.stringify(reply.user));
            return this._loggedUser.next(reply.user);
          }
          reject(new Error(reply.message));
        } )
    })
  }

  get loggedUser() {
    return this._loggedUser;
  }
}