import { Component, ViewChild, ViewContainerRef,TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FacebookService, FacebookLoginResponse, FacebookInitParams } from 'ng2-facebook-sdk';
import { SessionStore } from '../session/session.store';

import { Modal, VEXModalContext } from 'angular2-modal/plugins/vex';
import { Overlay, overlayConfigFactory } from 'angular2-modal';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [FacebookService]
})
export class NavbarComponent {
  @ViewChild('templateLoginModal') public templateLoginModal: TemplateRef<any>;
  @ViewChild('templateRegistroModal') public templateRegistroModal: TemplateRef<any>;
  public loginForm: FormGroup;
  public signUpForm: FormGroup;
  public loginProcess: boolean = false;
  public signUpProcess: boolean = false;
  public errorLogin: string = undefined;
  public errorRegistro: string = undefined;

  constructor(
    private fb: FacebookService,
    private sessionStore: SessionStore, 
    public modal: Modal,
    private formBuilder: FormBuilder,
    overlay: Overlay, 
    vcRef: ViewContainerRef) { 

    let fbParams: FacebookInitParams = {
      appId: '225598827837226',
      xfbml: true,
      version: 'v2.7'
    };

    this.loginForm = formBuilder.group({
      'email': ['giwirodavalos@gmail.com', Validators.required],
      'password': [null, Validators.required]
    });

    this.signUpForm = formBuilder.group({
      'nombres': [null, Validators.required],
      'apellidos': [null, Validators.required],
      'email': [null, Validators.required],
      'password': [null, Validators.required],
      'c_password': [null, Validators.required]
    });

    overlay.defaultViewContainer = vcRef;
    this.fb.init(fbParams);
  }

  private finishForms(dialogRef): void {
    dialogRef.dismiss();
    this.loginForm.reset();
    this.signUpForm.reset();
  }

  onLogin(value: any, dialogRef: any){
    let data = {
      'email' : value.email,
      'password' : value.password
    };
    this.loginProcess = true;
    this.sessionStore.performLogin(data)
      .then( (reply: any) => {
        this.finishForms(dialogRef);
      })
      .catch( (error: Error) => {
        this.errorLogin = error.message;
      })
      .finally( () => {
        this.loginProcess = false;
        this.errorLogin = '';
      })
  }

  goLoginModal(dialogRef: any) {
    dialogRef.dismiss();
    this.loginModal();
  }

  loginModal(event?: any) {
    this.modal
        .open(this.templateLoginModal, overlayConfigFactory({ isBlocking: false }, VEXModalContext));
  }

  onRegistro(value: any, dialogRef: any) {
    let data = {
      'nombre': value.nombres,
      'apellido': value.apellidos,
      'email': value.email,
      'password': value.password,
    }
    this.signUpProcess = true;
    this.sessionStore.performRegistro(data)
      .then( (reply: any) => {
        this.finishForms(dialogRef);
        console.log('onRegistro performRegistro', reply);
      })
      .catch( (error: Error) => {
        this.errorRegistro = error.message;
      })
      .finally( () => {
        this.signUpProcess = false;
        this.errorRegistro = '';
      });
  }

  goRegistroModal(dialogRef: any) {
    dialogRef.dismiss();
    this.registroModal();
  }

  registroModal(event?: any) {
    this.modal
        .open(this.templateRegistroModal, overlayConfigFactory({ isBlocking: false }, VEXModalContext));
  }

  onLoginFB(event: any, dialogRef: any) {
    this.loginProcess = true;
    this.fb.login().then(
      (response: FacebookLoginResponse) => {
        if (response.status != 'connected') {
          return console.log('status not connected', response.status);
        }

        this.sessionStore
          .performLoginFB(response.authResponse.accessToken)
          .then( (reply: any) => {
            this.finishForms(dialogRef);
            console.log('onLoginFB performLoginFB', reply);
          })
          .finally( () => {
            this.loginProcess = false;
          });
      },
      (error: any) => {
        this.loginProcess = false;
      }
    );
    event.preventDefault();
  }

  logout(event: any) {
    this.sessionStore.performLogout();
    event.preventDefault();
  }
}