/*import {Record} from 'immutable';

const UserRecord = Record({
    _id: undefined,
    title: '',
    url: '',
    thumbnail_url: ''
});*/

export interface IUser /*extends UserRecord*/ {

  _id: number;
  title: string;
  description: string; 
  thumbnail_url: string;

  nombre: string;
  apellido: string;
  dni: string;
  direccion: string;
  ciudad: string;
  telefono: string;
  foto: string;
  pais: string;
  descripcion: string;
  local: {
    email: string,
    activateToken: string,
    password: string
  };
  facebook: {
    token: string,
    fbid: string,
    email: string
  };
  viajero: boolean;
  cuentaPagos: {
    docId: string;
    nombreBanco: string;
    tipoCuenta: string;
    tipoMoneda: string;
    numeroCuenta: string;
    cciCuenta: string
  }

}
