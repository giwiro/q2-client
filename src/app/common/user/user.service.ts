
import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import { _buildHeaders, _buildSearchParams } from '../../utils/remoteUtil';

import { IUser } from './user.interface';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

// Form header
const formHeader = {
  'Content-Type': 'application/json; charset=UTF-8'
}

@Injectable()
export class UserService {

  //Moverlo a env
  ENDPOINT = 'http://www.giwiro.com:8080';

  constructor(private http:Http) { }

  private _createPostObservable<T>(url:String, data?:Object, headers?:Object): Observable<any> {

    const requestOptions = new RequestOptions({
      headers: _buildHeaders(headers ? headers : {}),
      withCredentials: true
    })

    return this.http
      .post(this.ENDPOINT + url, data ? data : {}, requestOptions)
      .map( (res:Response) => <T>res.json() )
      //.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  private _createGetObservable<T>(url:String, params?:Object, headers?:Object): Observable<any> {

    const requestOptions = new RequestOptions({
      headers: _buildHeaders(headers ? headers : {}),
      search: _buildSearchParams(params ? params : {})
    })

    return this.http
      .get(this.ENDPOINT + url, requestOptions)
      .map( (res:Response) => <T>res.json() )
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  serviceRegistro(params: Object) {
    return this._createPostObservable<IReplyRegistro>('/api/createUser', params);
  }
  
}

export interface IReplyRegistro {
  success: boolean,
  user: IUser,
  message: string
}