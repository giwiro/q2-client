import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { ModalModule } from 'angular2-modal';
import { VexModalModule } from 'angular2-modal/plugins/vex';

import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { MenuComponent } from './common/menu/menu.component';
import { FooterComponent } from './common/footer/footer.component';

import { SessionService } from './common/session/session.service';
import { UserService } from './common/user/user.service';
import { SessionStore } from './common/session/session.store';

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    NavbarComponent,
    MenuComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ModalModule.forRoot(),
    VexModalModule,
    RouterModule.forRoot(AppRoutes)
  ],
  providers: [
    CookieService,
    SessionService,
    SessionStore,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
