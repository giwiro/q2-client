import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';

const defaultHeaders: Object = {
  'device': 'mobile'
}

const _buildHeaders = (headers): Headers => {
  return new Headers(Object.assign({}, defaultHeaders, headers));
}

const _buildSearchParams = (params): URLSearchParams => {
  let searchParams = new URLSearchParams();
  for (let key in params) {
    searchParams.set(key, params[key]);
  }
  return searchParams;
}

export { _buildHeaders, _buildSearchParams };